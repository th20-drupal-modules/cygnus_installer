<?php

/**
 * @file
 * Defines module installation helpers.
 *
 * Copyright (C) 2014, Victor Nikulshin
 *
 * This file is part of 'Cygnus Installer' Drupal module.
 *
 * Cygnus Installer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Cygnus Installer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Cygnus Installer.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Configures creation and display properties of node types.
 *
 * Changes values of system variables related to the specified node type.
 */
function cygnus_installer_neutralize_node_settings($type, array $config = array()) {
  $config += array(
    'comments' => FALSE,
    'menu' => array(),
    'preview' => FALSE,
    'publish' => TRUE,
    'revision' => TRUE,
    'submitted' => FALSE,
    'translate' => FALSE,
  );

  if (!preg_match('/^[A-Za-z0-9_-]+$/', $type)) {
    // Invalid type format.
    return FALSE;
  }

  $options = array();
  if ($config['publish']) {
    $options[] = 'status';
  }
  if ($config['revision']) {
    $options[] = 'revision';
  }
  variable_set("node_options_{$type}", $options);

  variable_set("node_preview_{$type}", $config['preview'] ? 1 : 0);
  variable_set("node_submitted_{$type}", $config['submitted'] ? 1 : 0);
  variable_set("menu_options_{$type}", $config['menu']);
  variable_set("comment_{$type}", $config['comments'] ? COMMENT_NODE_OPEN : COMMENT_NODE_CLOSED);
  variable_set("language_content_type_{$type}", $config['translate'] ? 2 : 0);

  return TRUE;
}

/**
 * Creates and updates default views in the module directory.
 */
function cygnus_installer_create_views($module) {
  $path = drupal_get_path('module', $module) . '/views';
  if (is_dir($path)) {
    foreach (file_scan_directory($path, '/\.(php|view)$/') as $file) {
      $view = include $file->uri;

      $old_view = views_get_view($view->name);
      if (!empty($old_view)) {
        views_delete_view($old_view);
      }

      $view->save();
      watchdog('system', format_string('View %n has been created.', array('%n' => $view->name)));
    }
  }
}

/**
 * Creates and updates default entity fields in the module directory.
 */
function cygnus_installer_create_fields($module) {
  $path = drupal_get_path('module', $module) . '/fields';
  foreach (file_scan_directory($path, '/^[a-z][a-z0-9_]+$/', array('recurse' => FALSE)) as $field_dir) {
    $field_name = $field_dir->name;
    $field_path = $field_dir->uri;
    if (!is_file("$field_path/field.php")) {
      continue;
    }

    $field = include "$field_path/field.php";
    if (is_array($field)) {
      $field['field_name'] = $field_name;
      if (field_info_field($field['field_name'])) {
        field_update_field($field);
      }
      else {
        field_create_field($field);
      }
    }

    foreach (file_scan_directory($field_path, '/^instance\.(.+\.)?php$/') as $instance_file) {
      $instance = include $instance_file->uri;
      if (is_array($instance)) {
        $instance['field_name'] = $field_name;
        if (field_info_instance($instance['entity_type'], $instance['field_name'], $instance['bundle'])) {
          field_update_instance($instance);
        }
        else {
          field_create_instance($instance);
        }
      }
    }
  }
}

/**
 * Creates a taxonomy vocabulary and a set of terms.
 */
function cygnus_installer_create_taxonomy($vocab_name, $human_name, array $items = array()) {
  $vocab = taxonomy_vocabulary_machine_name_load($vocab_name);
  if ($vocab) {
    if ($vocab->name != $human_name) {
      $vocab->name = $human_name;
      taxonomy_vocabulary_save($vocab);
    }
  } else {
    if (!empty($human_name)) {
      $vocab = (object) array(
        'name' => $human_name,
        'machine_name' => $vocab_name,
      );
      taxonomy_vocabulary_save($vocab);
    }
  }

  if (!empty($vocab)) {
    _cygnus_installer_create_taxonomy_helper($vocab, $items);
  }
}

function _cygnus_installer_create_taxonomy_helper($vocab, &$items, $parent = NULL) {
  foreach ($items as $w => $i) {
    $term = (object) array(
      'vid' => $vocab->vid,
      'name' => $i['label'],
      'weight' => $w,
    );
    if ($parent) {
      $term->parent = $parent->tid;
    }
    taxonomy_term_save($term);
    $items[$w]['term'] = $term;
    if (!empty($i['items'])) {
      _cygnus_installer_create_taxonomy_helper($vocab, $i['items'], $term);
    }
  }
}
